package com.xsoftvn.wavprocesser.sound;

/**
 * Created by duc on 11/7/2014.
 */
public class WavInfo {
    public final short numChannels;
    public final int sampleRate;
    public final short bitsPerSample;
    public final short audioFormat;

    public WavInfo(short numChannels, int sampleRate, short bitsPerSample, short audioFormat) {
        this.numChannels = numChannels;
        this.sampleRate = sampleRate;
        this.bitsPerSample = bitsPerSample;
        this.audioFormat = audioFormat;
    }
}
