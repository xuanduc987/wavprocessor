package com.xsoftvn.wavprocesser.sound;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by duc on 11/6/2014.
 */
public class WavData {
    private WavInfo wavInfo;
    private double[][] samples;

    private WavData() {
    }

    public static WavData fromStream(InputStream stream) throws IOException {
        WavData wavData = new WavData();
        //Need to call in this order
        wavData.readRiff(stream);
        wavData.readFmt(stream);
        wavData.readData(stream);
        return wavData;
    }

    private static int little2Int(byte[] bytes) {
        return (bytes[0] & 0xFF) | ((bytes[1] & 0xFF) << 8) | ((bytes[2] & 0xFF) << 16) | ((bytes[3] & 0xFF) << 24);
    }

    private static short little2Short(byte[] bytes) {
        return (short) ((bytes[0] & 0xFF) | ((bytes[1] & 0xFF) << 8));
    }

    private void readRiff(InputStream stream) throws IOException {
        byte[] dWord = new byte[4];
        stream.read(dWord); //ChunkID
        stream.read(dWord); //ChunkSize
        stream.read(dWord); //Format
        int format = little2Int(dWord);
        if (format != 0x45564157) // 'WAV' = 0x57415645 big-endian
            throw new IllegalArgumentException("Not WAV file");
    }

    private void readFmt(InputStream stream) throws IOException {
        byte[] dWord = new byte[4];
        byte[] word = new byte[2];
        stream.read(dWord); //Subchunk1ID 'fmt'
        stream.read(dWord); //Subchunk1Size
        int fmtSize = little2Int(dWord);
        stream.read(word); //AudioFormat
        short format = little2Short(word);
        stream.read(word); //NumChannels
        short numChannels = little2Short(word);
        stream.read(dWord); //SampleRate
        int sampleRate = little2Int(dWord);
        stream.read(dWord); //ByteRate
        stream.read(word); //BlockAlign
        stream.read(word); //BitsPerSample
        short bitsPerSample = little2Short(word);
        //Read remain of the chunk
        int remain = fmtSize - 16;
        if (remain != 0) {
            byte[] temp = new byte[remain];
            stream.read(temp);
        }

        this.wavInfo = new WavInfo(numChannels, sampleRate, bitsPerSample, format);
    }

    private void readData(InputStream stream) throws IOException {
        byte[] dWord = new byte[4];
        stream.read(dWord); //Subchunk2Id 'data'
        stream.read(dWord); //Subchunk2Size
        int dataSize = little2Int(dWord);
        byte[] rawData = new byte[dataSize];
        stream.read(rawData);

        samples = new double[getWavInfo().numChannels][];
        int bytesPerSample = getWavInfo().bitsPerSample / 8;
        int samplesPerChannel = dataSize / getWavInfo().numChannels / bytesPerSample;
        for (int i = 0; i < getWavInfo().numChannels; ++i) {
            getSamples()[i] = new double[samplesPerChannel];
        }
        byte[] sampleBytes = new byte[bytesPerSample];
        int byteIndex = 0;
        double ratio = Math.pow(2., getWavInfo().bitsPerSample - 1);
        for (int s = 0; s < samplesPerChannel; ++s) {
            for (int c = 0; c < getWavInfo().numChannels; ++c) {
                for (int i = bytesPerSample - 1; i >= 0; --i) {
                    sampleBytes[i] = rawData[byteIndex++];
                }
                int sampleValue = 0;
                for (int i = 0; i < bytesPerSample; ++i) {
                    sampleValue += sampleBytes[i];
                    if (i < bytesPerSample - 1)
                        sampleValue <<= 8;
                }

                getSamples()[c][s] = sampleValue / ratio;
            }
        }
    }

    public WavInfo getWavInfo() {
        return wavInfo;
    }

    public double[][] getSamples() {
        return samples;
    }
}
