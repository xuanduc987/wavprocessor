package com.xsoftvn.wavprocesser;

import java.util.ArrayList;

/**
 * Created by duc on 11/9/2014.
 */
public class Utils {
    private static short WindowsSizeInMs = 30;
    public static int calculateWindowSize(int sampleRate) {
        int rawWindowSize = sampleRate * WindowsSizeInMs / 1000;
        int i = 0;
        while (rawWindowSize != 0) {
            rawWindowSize >>= 1;
            i++;
        }
        return 1 << (i - 1);
    }

    public static double[] adjustmentFilter(double x[]) {
        double[] y = new double[x.length];
        y[0] = x[0];
        for (int i = 1; i < x.length; i++) {
            y[i] = x[i] - 0.95*x[i-1];
        }
        return y;
    }

    //Khai Mai Tien

    public static double[] hamming(double[]x){
        double[] y = x.clone();
        int len = x.length;
        for (int i = 0; i < len; i++){
            y[i] = x[i]*(0.54 - 0.46*Math.cos(2*Math.PI*i/(len-1)));
        }
        return y;
    }

    public static void logArray(double[] x){
        for (int i = 0; i < x.length; i++)
            x[i] = Math.log10(x[i]);
    }

    public static double[] getCepstrum(double[] x){
        double[] y = x.clone();
        y = hamming(y);
        y = absFFT(y);
        logArray(y);
        double[] res = getRealInverseFFT(y);
        //res = splitArray(res, 0, res.length/2);
        return res;
    }

    private static double[] splitArray(double[] x, int start, int end){

        double[] res = new double[end-start];
        for (int i = start; i < end; i++)
            res[i] = x[i];
        return res;
    }

    public static double getFrequency(double[] ceptrum, double fs, float ratio1, float ratio2){
        int minThresh = 30;
        int n = ceptrum.length/2;
        ArrayList<Integer> maxIndex = new ArrayList<Integer>();
        for (int i = 1; i < n-1; i++)
            if (ceptrum[i] > 0 &&
                    ceptrum[i] > ceptrum[i+1] &&
                    ceptrum[i] > ceptrum[i-1] )
                maxIndex.add(i);
		/*find max value in these peaks*/
        int tmIndex = getMaxIndex(ceptrum, maxIndex, 0, maxIndex.size());
        int mIndex = maxIndex.get(tmIndex);
        //double maxValue = ceptrum[mIndex];
		/*find the max value on the left */
        int leftIndex = -1;
        double leftMax = 0;
        int tempIndex = 0;
        for (int i = 0; i < tmIndex; i++){
            tempIndex = maxIndex.get(i);
            if ( (mIndex - tempIndex) > minThresh && ceptrum[tempIndex] > leftMax){
                leftIndex = tempIndex;
                leftMax = ceptrum[tempIndex];
            }
        }

		/*find the max value on the right*/
        int rightIndex = -1;
        double rightMax = 0;
        for (int i = tmIndex + 1; i < maxIndex.size(); i++){
            tempIndex = maxIndex.get(i);
            if ( ( tempIndex-mIndex) > minThresh && ceptrum[tempIndex] > rightMax){
                rightIndex = tempIndex;
                rightMax = ceptrum[tempIndex];
            }
        }
		/*combine both left and right*/
        int seIndex = -1;
        if (rightIndex != -1 && leftIndex != -1){
            if (leftMax > rightMax)
                seIndex = leftIndex;
        }
        else if (rightIndex != -1){
            seIndex = rightIndex;
        } else if (leftIndex != -1){
            seIndex = leftIndex;
        }
        if (seIndex != -1)
            if (checkVoiced(ceptrum, maxIndex, mIndex, seIndex, ratio1, ratio2))
                return getFrequencyByN(Math.abs(mIndex-seIndex),fs);
        return -1;
    }

    private static double getFrequencyByN(int n, double fs){
        return fs/n;
        //return n;
    }

    private static boolean checkVoiced(double[] ceptrum, ArrayList<Integer> indexs, int firstIndex, int secondIndex, float ratio1, float ratio2){
		/*if the second is not quite different from the average of the others*/
        if (indexs.size() < 3)
            return true;
        double aver = 0;
        int temp = 0;
        int count = 0;
        int rad = 1;
        for (int i = 0; i < indexs.size(); i++){
            temp = indexs.get(i);
            if (outOfVin(firstIndex,rad,temp) && outOfVin(secondIndex,rad,temp)){
                aver += ceptrum[temp];
                count ++;
            }
        }
        aver = aver/count;
        double second = ceptrum[secondIndex];
        double first = ceptrum[firstIndex];
		/*compare the second with the average*/
        if (second/aver > ratio1 && first/aver > ratio2)
            return true;
        return false;
    }

    private static boolean outOfVin(int centre, int rad, int num){
        if (num > centre + rad)
            return true;
        if (num < centre - rad)
            return true;
        return false;
    }

    private static int getMaxIndex(double[] ceptrum, ArrayList<Integer> indexs, int start, int end){
        int index = -1;
        double temp = 0;
        int tempIndex = 0;
        for(int i = start; i < end; i++){
            tempIndex = indexs.get(i);
            if (ceptrum[tempIndex] > temp){
                temp = ceptrum[tempIndex];
                index = i;
            }
        }
        return index;
    }

    public static double[] absFFT(double[] x){
        int length = x.length;
        double[] y = x.clone();
        double[] im = new double[length];
        Fft.transform(y, im);
        return getResultFromX(y,im);
    }

    private static double[] getResultFromX(double[] re, double[] im){
        int len = re.length;

        double[] res =  new double[len];
        for (int i = 0; i < len; i++)
            res[i] = Math.sqrt(re[i]*re[i] + im[i]*im[i]);
        return res;
    }

    public static double[] reverseFFT(double[] x){
        int length = x.length;
        double[] y = x.clone();
        double[] im = new double[length];
        Fft.inverseTransform(y, im);
        return getResultFromX(y,im);
    }

    public static double[] getRealInverseFFT(double[] x){
        int length = x.length;
        double[] y = x.clone();
        double[] im = new double[length];
        Fft.inverseTransform(y, im);
        return y;
    }
}
