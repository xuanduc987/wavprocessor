package com.xsoftvn.wavprocesser;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.ScatterChart;
import com.github.mikephil.charting.data.*;
import com.xsoftvn.wavprocesser.settings.SettingsActivity;
import com.xsoftvn.wavprocesser.sound.WavData;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends ActionBarActivity {
    private static final int PICK_WAV_CODE = 1;
    private static final int MAX_SAMPLE = 16000;

    LineChart wavLineChart;
    LineData wavLineData;
    ArrayList<Entry> wavArray = new ArrayList<Entry>();
    LineDataSet wavDataSet = new LineDataSet(wavArray, "Wav");
    ArrayList<Entry> rectangleArray = new ArrayList<Entry>();
    LineDataSet rectangleDataSet = new LineDataSet(rectangleArray, "Window");
    LineChart cepstrumLineChart;
    LineData cepstrumLineData;
    ArrayList<Entry> cepstrumArray = new ArrayList<Entry>();
    LineDataSet cepstrumDataSet = new LineDataSet(cepstrumArray, "Cepstrum");
    ScatterChart f0ScatterChart;
    ScatterData f0ScatterData;
    ArrayList<Entry> f0Array = new ArrayList<Entry>();
    ScatterDataSet f0DataSet = new ScatterDataSet(f0Array, "F0");
    ArrayList<Entry> currentF0 = new ArrayList<Entry>();
    ScatterDataSet currentF0DataSet = new ScatterDataSet(currentF0, "Current F0");
    TextView textView;
    ProgressDialog progressDialog;

    private int windowSize = 0;
    private float halfWindowHeight = 0;
    private int currentIndex = -1;
    private double[] wavSamples = {0};
    private int fs = 0;

    private float ratio1;
    private float ratio2;

    private static void addArray2DataArray(double[] array, ArrayList<Entry> arrayList, int maxValue) {
        int step = 1;
        if (maxValue < array.length) {
            step = array.length/maxValue;
        }
        arrayList.clear();
        for (int i = 0; i < array.length; i+=step) {
            arrayList.add(new Entry((float) array[i], i));
        }
    }

    private static void addArray2DataArray(double[] array, ArrayList<Entry> arrayList) {
        addArray2DataArray(array, arrayList, Integer.MAX_VALUE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initCharts();
        initOtherViews();
        initWavData();
    }

    private void initOtherViews() {
        textView = (TextView) findViewById(R.id.info_text);
        textView.setText("F0 is unknown");
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        if (toolbar != null) {
            toolbar.inflateMenu(R.menu.toolbar_menu);
            toolbar.setTitle("Wav Processor");
            toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    switch (menuItem.getItemId()) {
                        case R.id.action_open:
                            Intent intent = new Intent();
                            intent.setType("audio/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(intent, PICK_WAV_CODE);
                            return true;
//                        case R.id.action_settings:
//                            Intent settingsIntent = new Intent(MainActivity.this, SettingsActivity.class);
//                            startActivity(settingsIntent);
//                            return true;
                        default:
                            break;
                    }
                    return false;
                }
            });
        }

        Button nextButton = (Button) findViewById(R.id.next_button);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (wavSamples.length < 5)
                    return;
                currentIndex = currentIndex == -1 ? 0 : currentIndex + windowSize / 2;
                if (currentIndex > wavSamples.length - 1) {
                    currentIndex -= windowSize / 2;
                    return;
                }
                int endIndex = currentIndex + windowSize - 1;
                if (endIndex > wavSamples.length - 1)
                    endIndex = wavSamples.length - 1;
                caculateWindowAndShow(currentIndex, endIndex, true);
            }
        });

        Button previousButton = (Button) findViewById(R.id.previous_button);
        previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (wavSamples.length < 5)
                    return;
                currentIndex = currentIndex - windowSize / 2;
                if (currentIndex < 0) {
                    currentIndex += windowSize / 2;
                    return;
                }
                int endIndex = currentIndex + windowSize - 1;
                caculateWindowAndShow(currentIndex, endIndex, true);
            }
        });

        Button analyzeButton = (Button)findViewById(R.id.analyze_button);
        analyzeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int backup = currentIndex;
                        if (wavSamples.length < 5)
                            return;
                        showProgressDialog();
                        for (;;) {
                            currentIndex = currentIndex == -1 ? 0 : currentIndex + windowSize / 2;
                            if (currentIndex > wavSamples.length - 1) {
                                currentIndex -= windowSize / 2;
                                break;
                            }
                            int endIndex = currentIndex + windowSize - 1;
                            if (endIndex > wavSamples.length - 1)
                                endIndex = wavSamples.length - 1;
                            caculateWindowAndShow(currentIndex, endIndex, false);
                        }
                        currentF0.clear();
                        currentIndex = backup;
                        hideProgressDialog();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                f0ScatterChart.invalidate();
                            }
                        });
                    }
                }).start();

            }
        });
    }

    private void initLineChartAttribute(LineChart chart) {
        chart.setDrawGridBackground(false);
        chart.setGridColor(Color.GRAY);
        chart.setDrawBorder(false);
        chart.getXLabels().setTextColor(Color.WHITE);
        chart.getYLabels().setTextColor(Color.WHITE);
        chart.setDrawLegend(false);
        chart.setDescription("");
    }

    private void initCharts() {
        wavLineChart = (LineChart)findViewById(R.id.wav_linechart);
        wavLineChart.setStartAtZero(false);
        initLineChartAttribute(wavLineChart);
        wavDataSet.setDrawCircles(false);
        wavDataSet.setDrawFilled(false);
        wavDataSet.setColor(Color.GREEN);
        rectangleDataSet.setColor(Color.RED);
        rectangleDataSet.setDrawCircles(false);
        rectangleDataSet.setDrawFilled(false);

        cepstrumLineChart = (LineChart)findViewById(R.id.cepstrum_linechart);
        cepstrumLineChart.setStartAtZero(false);
        initLineChartAttribute(cepstrumLineChart);
        cepstrumLineChart.setYRange(-100, 100, false);
        cepstrumLineChart.setDrawYValues(false);
        cepstrumDataSet.setColor(Color.GREEN);
        cepstrumDataSet.setDrawCircles(false);
        cepstrumDataSet.setDrawFilled(false);

        f0ScatterChart = (ScatterChart)findViewById(R.id.f0_scatterchart);
        f0ScatterChart.setDrawGridBackground(false);
        f0ScatterChart.setGridColor(Color.GRAY);
        f0ScatterChart.setDrawBorder(false);
        f0ScatterChart.getXLabels().setTextColor(Color.WHITE);
        f0ScatterChart.getYLabels().setTextColor(Color.WHITE);
        f0ScatterChart.setYRange(0f, 600f, false);
        f0ScatterChart.setDescription("");
        f0ScatterChart.setDrawYValues(false);
        f0ScatterChart.setUnit("Hz");
        f0DataSet.setScatterShape(ScatterChart.ScatterShape.CIRCLE);
        f0DataSet.setColor(Color.GREEN);
        f0DataSet.setScatterShapeSize(7f);

        currentF0DataSet.setScatterShape(ScatterChart.ScatterShape.CIRCLE);
        currentF0DataSet.setColor(Color.YELLOW);
        currentF0DataSet.setScatterShapeSize(10f);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putDoubleArray("wavSamples", wavSamples);
        outState.putInt("windowSize", windowSize);
        outState.putInt("fs", fs);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        wavSamples = savedInstanceState.getDoubleArray("wavSamples");
        windowSize = savedInstanceState.getInt("windowSize");
        fs = savedInstanceState.getInt("fs");
        initWavData();
    }

    private void caculateWindowAndShow(final int startIndex, final int endIndex, final boolean showData) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                addRectanglePoint(startIndex, endIndex);
                final double[] windowed = Arrays.copyOfRange(wavSamples, startIndex, endIndex + 1);
                double[] cepstrum = Utils.getCepstrum(Utils.adjustmentFilter(windowed));
                addArray2DataArray(Arrays.copyOf(cepstrum, cepstrum.length / 2), cepstrumArray, MAX_SAMPLE);
                final double f = Utils.getFrequency(cepstrum, fs, ratio1, ratio2);
                String text = "F0 is unknown";
                currentF0.clear();
                if (f > 0) {
                    text = "F0 = " + f +"Hz";
                    Entry entry = new Entry((float) f, (startIndex + endIndex) / 2);
                    if (!f0Array.contains(entry)) f0Array.add(entry);
                    currentF0.add(entry);
                }
                final String finalText = text;
                if (!showData) return;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textView.setText(finalText);
                    }
                });
                redrawAllGraph();
            }
        }).start();
    }

    private void redrawAllGraph() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                wavLineChart.invalidate();
                cepstrumLineChart.invalidate();
                f0ScatterChart.invalidate();
            }
        });
    }

    private void addRectanglePoint(int startIndex, int endIndex) {
        rectangleArray.clear();
        rectangleArray.add(new Entry(halfWindowHeight, startIndex));
        rectangleArray.add(new Entry(halfWindowHeight, endIndex));
        rectangleArray.add(new Entry(-halfWindowHeight, endIndex));
        rectangleArray.add(new Entry(-halfWindowHeight, startIndex));
        rectangleArray.add(new Entry(halfWindowHeight, startIndex));
    }

    @Override
    protected void onResume() {
        super.onResume();
        ratio1 = 3.5f;//Float.parseFloat(PreferenceManager.getDefaultSharedPreferences(this).getString("param_one", ""));
        ratio2 = 6f;//Float.parseFloat(PreferenceManager.getDefaultSharedPreferences(this).getString("param_two", ""));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == PICK_WAV_CODE) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            showProgressDialog();
                            InputStream stream = getContentResolver().openInputStream(data.getData());
                            final WavData wavData = WavData.fromStream(stream);
                            windowSize = Utils.calculateWindowSize(wavData.getWavInfo().sampleRate);
                            wavSamples = wavData.getSamples()[0];
                            fs = wavData.getWavInfo().sampleRate;
                            initWavData();
                            redrawAllGraph();

                            stream.close();
                        } catch (FileNotFoundException e) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(MainActivity.this, "File not found!", Toast.LENGTH_SHORT).show();
                                }
                            });
                        } catch (IOException e) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(MainActivity.this, "Cannot open file!", Toast.LENGTH_SHORT).show();
                                }
                            });
                        } catch (Exception e) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(MainActivity.this, "Can only read wav file!", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                        finally {
                            hideProgressDialog();
                        }
                    }
                }).start();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null)
                    return;
                progressDialog = ProgressDialog.show(MainActivity.this, "Running", "Please wait...", true);
            }
        });
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Closing App")
                .setMessage("Are you sure you want to exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

    private void hideProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog == null)
                    return;
                progressDialog.dismiss();
                progressDialog = null;
            }
        });
    }

    private void initWavData() {
        double max = wavSamples[0];
        double min = wavSamples[0];
        for (int i = 1; i < wavSamples.length; i++) {
            if (max < wavSamples[i]) max = wavSamples[i];
            if (min > wavSamples[i]) min = wavSamples[i];
        }
        max = Math.max(max, Math.abs(min));
        halfWindowHeight = (float)max * 1.1f;
        wavLineChart.setYRange(-halfWindowHeight * 1.1f, halfWindowHeight * 1.1f, false);

        String[] wavXVals = new String[wavSamples.length];
        for (int i = 0; i < wavSamples.length; i++) {
            wavXVals[i] = Integer.toString(i);
        }
        wavLineData = new LineData(wavXVals);
        addArray2DataArray(wavSamples, wavArray, MAX_SAMPLE);
        wavLineData.addDataSet(wavDataSet);
        rectangleArray.clear();
        wavLineData.addDataSet(rectangleDataSet);
        wavLineChart.setData(wavLineData);

        String[] cepstrumXVals = new String[windowSize/2];
        for (int i = 0; i < windowSize/2; i++) {
            cepstrumXVals[i] = Integer.toString(i);
        }
        cepstrumLineData = new LineData(cepstrumXVals);
        cepstrumArray.clear();
        cepstrumLineData.addDataSet(cepstrumDataSet);
        cepstrumLineChart.setData(cepstrumLineData);

        f0ScatterData = new ScatterData(wavXVals);
        f0ScatterChart.setData(f0ScatterData);
        f0Array.clear();
        currentF0.clear();
        f0ScatterData.addDataSet(f0DataSet);
        f0ScatterData.addDataSet(currentF0DataSet);
        currentIndex = -1;
    }
}